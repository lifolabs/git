# Version Control History

##  No version control system can eliminate the need for developers to talk to each other! 


## Version control Systems (VCS)

Local Version Control Systems (LVCS)
Centralized Version Control Systems (CVCSs)
Distributed Version Control Systems (DVCS)

## Local Version Control Systems (LVCS)

Copy files, dated directories
RCS, short for Revision Control System

Save a series of patches
Difficult to collaborate
Branching is almost impossible


## Centralized Version Control Systems (CVCS)

CVS, or Concurrent Versions System
Apache Subversion (aka. SVN)
Perforce
Rational Clear-Case (IBM) (Configuration Management)

Central server holds everything (good/bad)
Easier administration, better control
Limited or no off-line work
Single point of failure.
Branching is easy in some cases (Subversion)
Merging is still a pain

## Distributed Version Control Systems (DVCS)

Git)
Mercurial (hg)
GNU Bazaar (bzr)
Darcs

Full copy of repository
No central control (good/bad)
No single point of failure
Easy branching
Easy merging
Fast
Allow off-line development

## Why Git?

It seems to be far the most popular DVCS. 

## Git services

GitHub
GitLab
Bitbucket

## Git Overview

Snapshots of the objects (files) and not diffs. Using pointers eliminate duplications.
Nearly every operation is local. (Off-line work)
Integrity using SHA-1 hashes of the files. (40 character string of hexadecimal characters called "object name" or "SHA-1 id".)

## Git Install
On UNIX/Linux you use your package manger (apt-get, yum, etc...) or install from git-scm.
On Mac: git-scm or use Homebrew to install git.
On Microsoft Windows install Git from git-scm.

RedHat based: `yum install git-core`
Debian based: `apt-get install git-core`

## Command line

Linux: Any terminal or console

## Not Fan of Command line?

Many different GUI tools: gitk/gitg/gitkraken
Most of the tutorials are for the command line.
Most of the people who can provide you help understand the command line, but specific GUI tools.

## Which version do you have?


$ git --version
git version 2.17.0


## Configure Git

There are three levels of configuration:

System (--system)
User (--global)
Project (--local)


$ git config ...

On Unix

/etc/gitconfig
$HOME/.gitconfig (/home/foobar/.gitconfig)
.git/config


To access them use

--system
--global
--local

## Configure Git - personalize


$ git config --global --add user.name "Foo Bar"
$ git config --global --add user.email foo@bar.com

$ git config --list
$ git config --list --global
$ git config user.name      # to see specific value

## More configuration


$ git config --global --add alias.st status
$ git config --global --add alias.ci commit
$ git config --global --add alias.co checkout
$ git config --global --add alias.br branch

$ git config --global color.ui true
$ git config --global alias.lol "log --oneline --graph --decorate"
$ git config --global --add core.editor notepad
$ git config --global --add merge.tool vimdiff
$ git config --global --add alias.unstage "reset HEAD"

$ git config --global --unset core.editor

Getting help


$ git help             # listing the most important commands
$ git help COMMAND     # man page or local web page
$ git COMMAND --help   # the same

$ git help help        # help about the help system
$ git help --all       # list all the git commands
$ git help tutorial    # a simple git tutorial

