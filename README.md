# Version Control

Welcome to Version Control course

- [History](./00_history/README.md)
- [Introduction](./01_intro/README.md)
- [Basics](./02_basics/README.md)
- [Branching](./03_branching/README.md)
- [loca changes](./04_local/README.md)
- [remote changes](./05_remote/README.md)
- [workspace](./06_workspace/README.md)
- [workflows](./07_workflows/README.md)
- [flows](08_flow/README.md)
- [gitlab](./09_gitlab/README.md)
- [git internal](./10_git_internal/README.md)
- [exersices](./98_exercises/README.md)
- [homework](./99_homework/README.md)
