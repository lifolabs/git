# Exercises

1.

- Check if you already have Git installed (open command line ,check the version).
- Install Git.
- Open the command line.
- Check which version do you have?
- List the default configuration.
- Add your name, email to the global configuration.
- Look at the help page of one of the commands.


2.
- Create a directory and inside create a new local repository.
- Create a directory and in that directory create a file. (You can use Visual Studio or Eclipse or your IDE to start a new project.)
- Add the directories and files to the repository.
- Are there any files that should not be tracked?
- Make sure git will ignore them in this project.
- Make some changes. Check what are the changes. Commit some of them.
- Go over the previous chapter and execute all the commands we went through.


3.
- Create a tag on the current commit using git tag -a v1 -m 'this is v1'
- Use gitk --all to see it.
- Use git log to see it.
